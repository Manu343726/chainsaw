from pathlib import Path
import yaml


class TreeNodeKind(Enum):
    Directory = 0
    Map  = 1
    Leaf_array = 2
    Leaf_value = 3

class TreeNode:
    def __init__(self, name, kind, parent, data, filepath):
        self.name = name
        self.kind = kind
        self.children = {}
        self.parent = parent
        self.data = data
        self.filepath = filepath

    def __str__(self):
        if len(self.children) > 0:
            return ''

    @property
    def branch(self):
        if self.parent is None:
            return [self]
        else:
            return self.parent.branch.append(self)

    def find_leaf_by_branch(branch):
        if branch[0] != self.name:
            return None

        for name, child : self.children:
            leaf_following_child = child.find_leaf_by_branch(branch[1:])

            if leaf_following_child is not None:
                return leaf_following_child

        return None


def build_tree_from_filesystem(root_dir, parent_node = None):
    path = Path(root_dir)

    if path.is_file() and path.suffix == '.yml':
        return build_tree_from_yaml_file(root_dir, parent_node)
    elif not path.is_dir():
        return utils.return_error(None)('root path {} is not a directory or YAML file'.format(root_dir))

    node = TreeNode(kind=TreeNodeKind.Directory, parent=parent_node, name=path.stem, filepath=path)

    for subdir in path.iterdir():
        node.children[subdir.stem] = build_tree_from_filesystem(subdir, node)

    return node


def build_tree_from_yaml_file(yaml_file, parent_node = None):
    content = yaml.load(str(yaml_file))

    if isinstance(content, dict):
        return build_tree_from_yaml_map(build_tree_from_yaml_map(yaml_file, parent_node=node, map=content, name=yaml_file.stem))
    elif isinstance(content, list):
        return build_tree_from_yaml_array(yaml_file, parent_node=parent_node, array=content, name=yaml_file.stem)

def build_tree_from_yaml_map(yaml_file, parent_node, map, name):
    node = TreeNode(kind=TreeNodeKind.Map, name=name, parent=parent_node, filepath=yaml_file)

    for key, value in map:
        if isinstance(value, dict):
            node.children[key] = build_tree_from_yaml_map(yaml_file, parent_node=node, map=value, name=key);
        elif isinstance(value, list):
            node.children[key] = build_tree_from_yaml_array(yaml_file, parent_node=node, array=value, name=key);

    return node

def build_tree_from_yaml_array(yaml_file, parent_node, array, name):
    return TreeNode(kind=TreeNodeKind.Leaf_array, parent=parent_node, data=array, name=name, filepath=yaml_file)
